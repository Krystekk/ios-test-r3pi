import UIKit

protocol SBSelectCurrencyDelegate {
    func didSelectCurrency(_ currency: SBCurrency)
}

class SBSelectCurrencyViewController: UIViewController {

    lazy var apiClient: SBApiClientProtocol = SBApiClient()

    var dataSource = SBTableDataSource()
    var fetchCompletion: (() -> ())?
    var delegate: SBSelectCurrencyDelegate?
    
    func currencyView() -> SBCurrencyView {
        return view as! SBCurrencyView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDataSource()
        fetchCurrency()
        title = dataSource.list?.listTitle()
        currencyView().currencyTableView.delegate = self
    }
    
    func setupDataSource() {
        currencyView().currencyTableView.dataSource = dataSource
    }
    
    func fetchCurrency() {
        apiClient.loadCurrencies { (currencies, success) in
            if currencies != nil {
                self.dataSource.setList(list: SBCurrencyList(items: currencies!))
                self.currencyView().reloadViews(success: success)
                self.fetchCompletion?()
            }
        }
    }
}

extension SBSelectCurrencyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SBBasket.sharedInstance.currency = dataSource.list?.item(indexPath: indexPath) as! SBCurrency
        delegate?.didSelectCurrency(SBBasket.sharedInstance.currency)
        navigationController?.popViewController(animated: true)
    }
}
