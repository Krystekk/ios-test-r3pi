import UIKit

class SBCurrencyViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var selectedImageView: UIImageView!
    
    static var currencyViewCellID = "CurrencyViewCellID"
}
