import UIKit

struct SBCurrency {
    
    let code: String
    let exchangeRate: Float
    
    //MARK: Class methods
    
    static func parseJSON(json: [String : Float]) -> [SBCurrency] {
        var currencies = [SBCurrency]()
        for (key, jsonCurrency) in json {
            var key = key
            let range = key.startIndex..<key.index(key.startIndex, offsetBy: 3)
            key.removeSubrange(range)
            
            currencies.append(SBCurrency.init(code: key, exchangeRate: jsonCurrency))
        }
        return currencies.sorted(by: { $0.code < $1.code })
    }
    
    static func USD() -> SBCurrency {
        return SBCurrency(code: "USD", exchangeRate: 1.0)
    }

}

extension SBCurrency: Equatable {}

func ==(lhs: SBCurrency, rhs: SBCurrency) -> Bool {
    return lhs.code == rhs.code && lhs.exchangeRate == rhs.exchangeRate
}

func !=(lhs: SBCurrency, rhs: SBCurrency) -> Bool {
    return lhs.code != rhs.code || lhs.exchangeRate != rhs.exchangeRate
}
