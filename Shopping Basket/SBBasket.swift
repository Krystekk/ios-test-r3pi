import UIKit

class SBBasket: NSObject {
    
    static let sharedInstance : SBBasket = {
        let instance = SBBasket()
        return instance
    }()
    
    var currency = SBCurrency.USD()
    
    func convertUSDToCurrentCurrency(price: Float) -> Float {
        let convertedValue = currency.exchangeRate * price
        return Float(round(100 * convertedValue) / 100)
    }
    
    func productPrice(product: SBProduct) -> String {
        return String(convertUSDToCurrentCurrency(price: product.price)) + " " + currency.code + " per " + product.unit
    }
    
    func totalPrice(totalPrice: Float) -> String {
        return "Pay \(convertUSDToCurrentCurrency(price: totalPrice)) \(currency.code)"
    }
}
