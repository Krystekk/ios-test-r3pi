import Foundation

class SBProduct {
    let name: String
    let unit: String
    let price: Float
    var quantity = 0
    
    init(name: String, unit: String, price: Float) {
        self.name = name
        self.unit = unit
        self.price = price
    }
    
    func minus() {
        if quantity > 0 {
            quantity -= 1
        }
    }
    
    func plus() {
        quantity += 1
    }
}
