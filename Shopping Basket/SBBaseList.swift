import UIKit

class SBBaseList: NSObject {

    var items: [Any]
    var attributes = SBListAttributes.empty()
    
    override init() {
        self.items = []
    }
    
    init(items: [Any]) {
        self.items = items
    }
    
    //MARK: listProtocol
    
    func count() -> Int {
        return items.count
    }
    
    func item(indexPath: IndexPath) -> Any {
        return items[indexPath.row]
    }
    
    func margin() -> Float {
        return attributes.margin
    }
    
    func columns() -> Int {
        return attributes.columns
    }
    
    func listTitle() -> String {
        return ""
    }
    
    //MARK: Class methods
    
    class func emptyList() -> SBBaseList {
        return SBBaseList()
    }
}
