import Foundation

protocol SBApiClientProtocol {
    func loadCurrencies(completion: @escaping ([SBCurrency]?, Bool?) -> ())
}

class SBApiClient: SBApiClientProtocol {
    
    static var currencyBaseUrl = "https://secure-anchorage-50436.herokuapp.com/currency/show"
    static var secretKey = "Currency layer key"
    
    func loadCurrencies(completion: @escaping ([SBCurrency]?, Bool?) -> ()) {
        if let url = URL(string: SBApiClient.currencyBaseUrl) {
            var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
            request.httpMethod = "GET"
            startDownload(request: request, completion: completion)
        }
    }
    
    func startDownload(request: URLRequest, completion: @escaping ([SBCurrency]?, Bool?) -> ()) {
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            var success = error == nil
            var currencies = [SBCurrency]()
            if (success) {
                if let json = try? JSONSerialization.jsonObject(with: data!) as? [String:Any],
                    let success = json?["success"] as? Bool,
                    let quotes = json?["quotes"] as? [String:Float], success == true && quotes.count > 0 {
                        currencies = SBCurrency.parseJSON(json: quotes)
                } else {
                    success = false
                }
            }
            
            DispatchQueue.main.async {
                completion(currencies, success)
            }
        }
        dataTask.resume()
    }
}
