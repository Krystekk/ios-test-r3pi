import UIKit

class SBDataSource:NSObject, UICollectionViewDataSource {

    var list: SBBaseList?
    var basket = SBBasket.sharedInstance
    
    func setList(list: SBBaseList) {
        self.list = list
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list?.count() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SBProductViewCell.productCellID, for: indexPath) as! SBProductViewCell
        
        if let product = list?.item(indexPath: indexPath) as? SBProduct {
            setupCell(cell: cell, product: product)
        }
        return cell
    }
    
    func setupCell(cell: SBProductViewCell, product: SBProduct) {
        cell.productImageView.image = UIImage.init(named: product.name.lowercased())
        cell.productName.text = product.name
        cell.productPrice.text = basket.productPrice(product: product)
        cell.quantityLabel.text = "Quantity: " + String(product.quantity)
    }
}
