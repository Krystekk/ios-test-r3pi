import UIKit

protocol SBSummaryViewControllerDelegate {
    func didSuccessPay()
}

class SBSummaryViewController: UIViewController {
    
    var delegate: SBSummaryViewControllerDelegate?
    var totalPrice: Float = 0.0
    
    func summaryView() -> SBSummaryView {
        return view as! SBSummaryView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
    }
    
    func setupButton() {
        summaryView().payButton.setTitle(SBBasket.sharedInstance.totalPrice(totalPrice: totalPrice), for: .normal)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func payPressed(_ sender: Any) {
        delegate?.didSuccessPay()
        dismiss(animated: true, completion: nil)
    }
}
