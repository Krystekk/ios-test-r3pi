import UIKit

class SBTableDataSource: NSObject, UITableViewDataSource {
    
    var list: SBBaseList? = SBCurrencyList()
    var basket = SBBasket.sharedInstance
    
    func setList(list: SBBaseList) {
        self.list = list
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SBCurrencyViewCell.currencyViewCellID, for: indexPath) as! SBCurrencyViewCell
        
        if let currency = list?.item(indexPath: indexPath) as? SBCurrency {
            setupCell(cell: cell, currency: currency)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list?.count() ?? 0
    }
    
    func setupCell(cell: SBCurrencyViewCell, currency: SBCurrency) {
        cell.title.text = currency.code + " - " + String(currency.exchangeRate)
        cell.selectedImageView.isHidden = basket.currency != currency
    }

}
