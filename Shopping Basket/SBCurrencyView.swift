import UIKit

class SBCurrencyView: UIView {
    @IBOutlet var currencyTableView: UITableView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    
    let errorText = "Something went wrong"
    
    func reloadViews(success: Bool?) {
        indicatorView.stopAnimating()
        
        if success != nil, success == true {
            imageView.isHidden = true
            currencyTableView.reloadData()
        } else {
            titleLabel.text = errorText
            titleLabel.isHidden = false
        }
    }
}
