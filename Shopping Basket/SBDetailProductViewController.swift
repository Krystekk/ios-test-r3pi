import UIKit

protocol SBDetailProductDelegate {
    func didChangeQuantity(_ product: SBProduct?)
}

class SBDetailProductViewController: UIViewController {

    var product: SBProduct?
    var delegate: SBDetailProductDelegate?
    
    func detailProductView() -> SBDetailProductView {
        return view as! SBDetailProductView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        detailProductView().show(show:true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailProductView().configure()
        
        if let product = product {
            detailProductView().imageView.image = UIImage.init(named: (product.name.lowercased()))
            detailProductView().titleLabel.text = product.name
            detailProductView().priceLabel.text = SBBasket.sharedInstance.productPrice(product: product)
            detailProductView().quantityLabel.text = String(describing: product.quantity)
        }
    }

    @IBAction func okPressed(_ sender: Any) {
        detailProductView().show(show: false) { (Bool) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func minusPressed(_ sender: Any) {
        product?.minus()
        updateQuantityLabel()
    }
    
    @IBAction func plusPressed(_ sender: Any) {
        product?.plus()
        updateQuantityLabel()
    }
    
    func updateQuantityLabel() {
        if let product = product {
            detailProductView().quantityLabel.text = String(describing: product.quantity)
            delegate?.didChangeQuantity(product)
        }
    }
    
}
