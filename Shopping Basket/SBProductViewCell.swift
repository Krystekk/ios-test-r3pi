import UIKit

class SBProductViewCell: UICollectionViewCell {
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var productPrice: UILabel!
    @IBOutlet var productName: UILabel!
    @IBOutlet var quantityLabel: UILabel!
    
    static var productCellID = "productCellID"
}
