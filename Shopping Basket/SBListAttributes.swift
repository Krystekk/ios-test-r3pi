import Foundation

struct SBListAttributes {
    let columns: Int
    let margin: Float
    
    static func empty() -> SBListAttributes {
        return SBListAttributes(columns: 0, margin: 0.0)
    }
}
