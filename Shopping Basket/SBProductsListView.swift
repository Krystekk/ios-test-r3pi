import UIKit

class SBProductsListView: UIView {

    @IBOutlet var productsCollectionView: UICollectionView!
    @IBOutlet var checkoutButton: UIButton!
    
    func setEnableCheckoutButton(enable: Bool) {
        checkoutButton.isEnabled = enable
        checkoutButton.alpha = enable ? 1.0 : 0.5
    }
    
}
