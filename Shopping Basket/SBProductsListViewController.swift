import UIKit

class SBProductsListViewController: UIViewController, SBSelectCurrencyDelegate, SBDetailProductDelegate, SBSummaryViewControllerDelegate {
    
    var dataSource = SBDataSource()
    
    static var segueCurrencyIdentifier = "currencySegue"
    static var segueProductDetail = "productDetailSegue"
    static var segueSummary = "summarySegue"
    
    func productListView() -> SBProductsListView {
        return view as! SBProductsListView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        
        title = dataSource.list?.listTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateButtonEnable()
        setCheckoutButtonTitle()
    }

    private func setupDataSource() {
        dataSource.setList(list: SBProductList.foodList())
        productListView().productsCollectionView.dataSource = dataSource
        
        if let flow = productListView().productsCollectionView.collectionViewLayout as? SBCollectionViewFlowLayout {
            flow.setupItemSize(columns: dataSource.list!.columns(), margin: dataSource.list!.margin(), width: Float(view.bounds.width))
        }
    }
    
    //MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SBProductsListViewController.segueCurrencyIdentifier {
            prepareSegueForCurrencies(for: segue)
        } else if segue.identifier == SBProductsListViewController.segueProductDetail {
            prepareSegueForDetails(for: segue, sender: sender)
        } else if segue.identifier == SBProductsListViewController.segueSummary {
            prepareSegueForSummary(for: segue)
        }
    }
    
    private func prepareSegueForCurrencies(for segue: UIStoryboardSegue) {
        if let viewController = segue.destination as? SBSelectCurrencyViewController {
            viewController.delegate = self
        }
    }
    
    private func prepareSegueForDetails(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? SBDetailProductViewController {
            let view = sender as! UICollectionViewCell
            if let indexPath = productListView().productsCollectionView.indexPath(for: view) {
                viewController.product = dataSource.list?.item(indexPath: indexPath) as? SBProduct
                viewController.delegate = self
            }
        }
    }
    
    private func prepareSegueForSummary(for segue: UIStoryboardSegue) {
        if let viewController = segue.destination as? SBSummaryViewController {
            viewController.delegate = self
            if let productList = dataSource.list as? SBProductList {
                viewController.totalPrice = productList.totalPrice()
            }
            
        }
    }

    //MARK: Button
    
    private func updateButtonEnable() {
        if let productList = dataSource.list as? SBProductList {
            productListView().setEnableCheckoutButton(enable: productList.productCount() > 0)
            setCheckoutButtonTitle()
        }
    }
    
    private func setCheckoutButtonTitle() {
        if let productList = dataSource.list as? SBProductList {
            productListView().checkoutButton.setTitle(productList.productCount() == 0 ? "Checkout" : "Checkout (\(productList.productCount()))", for: .normal)
        }
    }
    
    //MARK: Delegates
    
    func didSelectCurrency(_ currency: SBCurrency) {
        productListView().productsCollectionView.reloadData()
    }
    
    func didChangeQuantity(_ product: SBProduct?) {
        productListView().productsCollectionView.reloadData()
        updateButtonEnable()
    }
    
    func didSuccessPay() {
        if let productList = dataSource.list as? SBProductList {
            dataSource.setList(list: SBProductList.foodList())
            productListView().productsCollectionView.reloadData()
            
            productListView().setEnableCheckoutButton(enable: productList.productCount() > 0)
            setCheckoutButtonTitle()
        }
    }
}
