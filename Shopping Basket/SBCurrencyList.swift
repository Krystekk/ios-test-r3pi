import UIKit

class SBCurrencyList: SBBaseList {

    static var currencyListTitle = "Currencies"
    
    //MARK: listProtocol
    
    override func listTitle() -> String {
        return SBCurrencyList.currencyListTitle
    }
}
