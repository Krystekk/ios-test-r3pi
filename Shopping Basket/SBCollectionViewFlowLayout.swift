import UIKit

class SBCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    func setupItemSize(columns: Int, margin: Float, width: Float) {
        let width = getWidthForCellItem(columns: columns, margin: margin, width: width)
        itemSize = CGSize(width: width, height: width * 1.4)
        
        let margin = CGFloat(margin)
        sectionInset = UIEdgeInsetsMake(margin, margin, margin, margin)
        
        minimumInteritemSpacing = margin
    }

    func getWidthForCellItem(columns: Int, margin: Float, width: Float) -> Double {
        return Double((width - (Float((columns + 1)) * margin)) / Float(columns))
    }
}
