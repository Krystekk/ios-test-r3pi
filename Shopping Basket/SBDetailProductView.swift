import UIKit

class SBDetailProductView: UIView {

    @IBOutlet weak var detailBackground: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setBlur()
        alpha = 0.0
    }
    
    func configure() {
        setupDetailBackground()
        setBorder(button: plusButton)
        setBorder(button: minusButton)
    }
    
    func show(show: Bool, completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: 0.2, animations: { 
            self.alpha = show ? 1.0 : 0.0
        }, completion: completion)
    }
    
    private func setBlur() {
        let blur = UIBlurEffect(style: .light)
        let effectView = UIVisualEffectView(effect: blur)
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(effectView, at: 0)
    }
    
    private func setBorder(button: UIButton) {
        button.layer.borderWidth = 1.0
        button.layer.borderColor = button.titleLabel?.textColor.cgColor
    }
    
    private func setupDetailBackground() {
        detailBackground.layer.cornerRadius = 5
        
        detailBackground.layer.shadowColor = UIColor.black.cgColor
        detailBackground.layer.shadowOpacity = 0.5
        detailBackground.layer.shadowOffset = CGSize.zero
        detailBackground.layer.shadowRadius = 5
    }
}
