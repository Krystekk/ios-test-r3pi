import Foundation

class SBProductList: SBBaseList {

    static var productListTitle = "Basket"
    
    func productCount() -> Int {
        var count = 0
        for (_, element) in items.enumerated() {
            let product = element as! SBProduct
            count += product.quantity
        }
        return count
    }
    
    func totalPrice() -> Float {
        var total = Float(0.0)
        for (_, element) in items.enumerated() {
            let product = element as! SBProduct
            total += (product.price * Float(product.quantity))
        }
        return total
    }
    
    //MARK: listProtocol

    override func listTitle() -> String {
        return SBProductList.productListTitle
    }
    
    //MARK: Class methods
    
    class func foodList() -> SBProductList {
        let columnsCount = 2
        let productList = SBProductList(items: [
                SBProduct(name: "Peas", unit: "bag", price: 0.95),
                SBProduct(name: "Eggs", unit: "dozen", price: 2.10),
                SBProduct(name: "Milk", unit: "bottle", price: 1.30),
                SBProduct(name: "Beans", unit: "can", price: 0.73)
            ])
        productList.attributes = SBListAttributes(columns: columnsCount, margin: SBSize.standardMargin)
        
        return productList
    }
}
