import XCTest
@testable import Shopping_Basket

class SBProductTests: XCTestCase {
    
    var product: SBProduct!
    
    override func setUp() {
        product = SBProduct(name: "Eggs", unit: "dozen", price: 2.10)
    }
    
    func testShouldTakeProductName() {
        XCTAssertEqual(product.name, "Eggs")
    }
    
    func testShouldTakeProductUnit() {
        XCTAssertEqual(product.unit, "dozen")
    }
    
    func testShouldTakeProductPrice() {
        XCTAssertEqual(product.price, 2.10)
    }
    
    func testShouldTakeProductImageName() {
        XCTAssertNotNil(UIImage.init(named: product.name.lowercased()))
    }
    
    func testMinusQuantity() {
        product.minus()
        XCTAssertEqual(0, product.quantity)
        product.quantity = 5
        
        product.minus()
        XCTAssertEqual(4, product.quantity)
    }

    func testPlusQuantity() {
        product.plus()
        XCTAssertEqual(1, product.quantity)
    }
    
}
