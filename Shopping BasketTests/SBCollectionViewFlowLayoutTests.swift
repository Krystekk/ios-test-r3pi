import XCTest
@testable import Shopping_Basket

class SBCollectionViewFlowLayoutTests: XCTestCase {

    var sut: SBProductsListViewController!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "ProductsListViewController") as! SBProductsListViewController
        _ = sut.view
    }
    
    func testItemCellSize() {
        let flow = sut.productListView().productsCollectionView.collectionViewLayout as! SBCollectionViewFlowLayout
        let widthItem = flow.getWidthForCellItem(columns: sut.dataSource.list!.columns(), margin: sut.dataSource.list!.margin(), width: Float(sut.view.bounds.width))
        XCTAssertEqual(flow.itemSize, CGSize(width: widthItem, height: widthItem * 1.4))
    }
    
    func testCollectionPaddingIsSet() {
        let flow = sut.productListView().productsCollectionView.collectionViewLayout as! SBCollectionViewFlowLayout
        let margin = CGFloat(sut.dataSource.list!.margin())
        XCTAssertEqual(flow.sectionInset, UIEdgeInsetsMake(margin, margin, margin, margin))
    }
    
    func testCollectionMinimumInteritemSpacingNotZero() {
        let flow = sut.productListView().productsCollectionView.collectionViewLayout as! SBCollectionViewFlowLayout
        XCTAssertEqual(flow.minimumInteritemSpacing, CGFloat(sut.dataSource.list!.margin()))
    }
    
}
