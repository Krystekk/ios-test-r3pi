import XCTest
@testable import Shopping_Basket

class SBCollectionDataSourceTests: XCTestCase {
    
    var dataSource: SBDataSource!
    var collection: UICollectionView!
    
    override func setUp() {
        dataSource = SBDataSource()
        collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    func testReturnFourRowsForFourProducts() {
        dataSource.setList(list: SBProductList.foodList())
        XCTAssertEqual(dataSource.collectionView(collection, numberOfItemsInSection: 0), SBProductList.foodList().count())
    }
    
    func testReturnZeroRowsForEmptyProducts() {
        dataSource.setList(list: SBProductList.emptyList())
        XCTAssertEqual(dataSource.collectionView(collection, numberOfItemsInSection: 0), SBProductList.emptyList().count())
    }
}
