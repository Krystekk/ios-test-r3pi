import XCTest
@testable import Shopping_Basket

class SBDetailProductViewControllerTests: XCTestCase {

    var sut: SBDetailProductViewController!
    var product: SBProduct!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        sut = storyboard.instantiateViewController(withIdentifier: "DetailProductViewController") as! SBDetailProductViewController
        
        product = SBProduct(name: "Eggs", unit: "KG", price: 9.99)
        sut.product = product
        
        _ = sut.view
    }
    
    func testControllerComponentsNotNil() {
        XCTAssertNotNil(sut.detailProductView().detailBackground)
        XCTAssertNotNil(sut.detailProductView().titleLabel)
        XCTAssertNotNil(sut.detailProductView().priceLabel)
        XCTAssertNotNil(sut.detailProductView().imageView)
        XCTAssertNotNil(sut.detailProductView().minusButton)
        XCTAssertNotNil(sut.detailProductView().plusButton)
        XCTAssertNotNil(sut.detailProductView().quantityLabel)
        XCTAssertNotNil(sut.detailProductView().saveButton)
    }
    
    func testSetupDetails() {
        
        XCTAssertNotNil(sut.detailProductView().detailBackground)
        XCTAssertNotNil(sut.detailProductView().imageView.image)
    
        let basket = SBBasket()
        XCTAssertEqual(sut.detailProductView().priceLabel.text, basket.productPrice(product: product))
        XCTAssertEqual(sut.detailProductView().titleLabel.text, String(product.name))
        XCTAssertEqual(sut.detailProductView().quantityLabel.text, "0")
    }
    
    func testMinusButton() {
        product.quantity = 5
        sut.minusPressed(sut.detailProductView().minusButton)
        XCTAssertEqual(4, product.quantity)
        XCTAssertEqual(sut.detailProductView().quantityLabel.text, String(product.quantity))
    }
    
    func testPlusButton() {
        product.quantity = 5
        sut.plusPressed(sut.detailProductView().minusButton)
        XCTAssertEqual(sut.detailProductView().quantityLabel.text, String(product.quantity))
    }
    
}
