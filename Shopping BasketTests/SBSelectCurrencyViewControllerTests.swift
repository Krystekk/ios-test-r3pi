import XCTest
@testable import Shopping_Basket

class SBSelectCurrencyViewControllerTests: XCTestCase {
    
    var sut: SBSelectCurrencyViewController!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "SelectCurrencyViewController") as! SBSelectCurrencyViewController
        _ = sut.view
    }
    
    func testControllerComponentsNotNil() {
        XCTAssertNotNil(sut.currencyView().currencyTableView)
        XCTAssertNotNil(sut.currencyView().imageView)
        XCTAssertNotNil(sut.currencyView().imageView)
        XCTAssertNotNil(sut.currencyView().indicatorView)
    }
    
    func testCurrenciesControllerTitle() {
        XCTAssertEqual(sut.title, "Currencies")
    }
    
    func testCellComponentsNotNil() {
        sut.dataSource.basket.currency = SBCurrency.PLN()
        
        let mockApiClient = SBMockApiClient()
        sut.apiClient = mockApiClient
        sut.apiClient.loadCurrencies { (currencies: [SBCurrency]?, success: Bool?) in
            
            self.sut.dataSource.setList(list: SBCurrencyList(items: currencies!))
            
            let firstCurrencyIndexPath = IndexPath(row: 0, section: 0)
            let cell = self.sut.dataSource.tableView(self.sut.currencyView().currencyTableView, cellForRowAt: firstCurrencyIndexPath) as! SBCurrencyViewCell
            
            XCTAssertNotNil(cell.title)
            XCTAssertNotNil(cell.selectedImageView)
            
            XCTAssertEqual(cell.title.text, "PLN - 1.12321")
            XCTAssertEqual(cell.selectedImageView.isHidden, false)
            
            let secondCurrencyIndexPath = IndexPath(row: 1, section: 0)
            let secondCell = self.sut.dataSource.tableView(self.sut.currencyView().currencyTableView, cellForRowAt: secondCurrencyIndexPath) as! SBCurrencyViewCell
            XCTAssertEqual(secondCell.selectedImageView.isHidden, true)
        }
    }
    
    func testCallLoadCurriences() {
        let mockApiClient = SBMockApiClient()
        sut.apiClient = mockApiClient
        sut.apiClient.loadCurrencies { (currencies: [SBCurrency]?, success: Bool?) in
            XCTAssertNotNil(currencies)
        }
    }
    
    func testFetchCurriences() {
        let fetchExpectation = expectation(description: "Fetch curriences")
        sut.fetchCompletion = { fetchExpectation.fulfill() }
        
        waitForExpectations(timeout: 60) { (error) in
            XCTAssertEqual(self.sut.currencyView().currencyTableView.numberOfRows(inSection: 0), 169)
        }
    }
    
    func testVisibleErrorText() {
        sut.currencyView().reloadViews(success: false)
        XCTAssertEqual(self.sut.currencyView().indicatorView.isHidden, true)
        XCTAssertEqual(self.sut.currencyView().indicatorView.isAnimating, false)
        XCTAssertEqual(self.sut.currencyView().titleLabel.isHidden, false)
    }
    
    func testVisibleTableView() {
        XCTAssertEqual(sut.currencyView().indicatorView.isHidden, false)
        XCTAssertEqual(sut.currencyView().indicatorView.isAnimating, true)
        XCTAssertEqual(sut.currencyView().imageView.isHidden, false)
        XCTAssertEqual(sut.currencyView().titleLabel.isHidden, true)
        
        sut.currencyView().reloadViews(success: true)
        
        XCTAssertEqual(self.sut.currencyView().indicatorView.isHidden, true)
        XCTAssertEqual(self.sut.currencyView().indicatorView.isAnimating, false)
        XCTAssertEqual(self.sut.currencyView().imageView.isHidden, true)
        XCTAssertEqual(self.sut.currencyView().titleLabel.isHidden, true)
    }
    
    func testTableDelegateNotNil() {
        XCTAssertNotNil(sut.currencyView().currencyTableView.delegate)
    }
}

extension SBSelectCurrencyViewControllerTests {
    class SBMockApiClient: SBApiClientProtocol {
        
        func loadCurrencies(completion: @escaping ([SBCurrency]?, Bool?) -> ()) {
            completion([SBCurrency.PLN(), SBCurrency.USD()], true)
        }
    }
}
