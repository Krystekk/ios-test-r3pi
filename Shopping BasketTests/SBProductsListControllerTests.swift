import XCTest
@testable import Shopping_Basket

class SBProductsListControllerTests: XCTestCase {

    var sut: SBProductsListViewController!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "ProductsListViewController") as! SBProductsListViewController
        _ = sut.view
    }
    
    func testProductsCollectionViewNotNil() {
        XCTAssertNotNil(sut.productListView().productsCollectionView)
    }
    
    func testCheckoutButtonNotNil() {
        XCTAssertNotNil(sut.productListView().checkoutButton)
    }
    
    func testFetchProducts() {
        XCTAssertEqual(sut.productListView().productsCollectionView.numberOfItems(inSection: 0), SBProductList.foodList().count())
    }
    
    func testCellComponentsNotNil() {
        let firstProductIndexPath = IndexPath(row: 0, section: 0)
        let cell = sut.dataSource.collectionView(sut.productListView().productsCollectionView, cellForItemAt: firstProductIndexPath) as! SBProductViewCell
        
        XCTAssertNotNil(cell.productImageView)
        XCTAssertNotNil(cell.productPrice)
        XCTAssertNotNil(cell.productName)
        XCTAssertNotNil(cell.quantityLabel)
    }

    func testCellComponentsSetup() {
        let firstProductIndexPath = IndexPath(row: 0, section: 0)
        let cell = sut.dataSource.collectionView(sut.productListView().productsCollectionView, cellForItemAt: firstProductIndexPath) as! SBProductViewCell
        
        let product = sut.dataSource.list!.item(indexPath: firstProductIndexPath) as! SBProduct
        
        XCTAssertNotNil(cell.productImageView.image)
        XCTAssertEqual(cell.productImageView.contentMode, UIViewContentMode.scaleAspectFit)
        
        let basket = SBBasket()
        XCTAssertEqual(cell.productPrice.text, basket.productPrice(product: product))
        XCTAssertEqual(cell.productName.text, String(product.name))
        XCTAssertEqual(cell.quantityLabel.text, "Quantity: 0")
    }
    
    func testShouldUpdatePrice() {
        let firstProductIndexPath = IndexPath(row: 0, section: 0)
        let product = sut.dataSource.list!.item(indexPath: firstProductIndexPath) as! SBProduct
        
        let basket = SBBasket()
        basket.currency = SBCurrency.PLN()
        let cell = sut.dataSource.collectionView(sut.productListView().productsCollectionView, cellForItemAt: firstProductIndexPath) as! SBProductViewCell
        XCTAssertEqual(cell.productPrice.text, basket.productPrice(product: product))
    }
    
    func testCheckoutBarButtonItemsNotNil() {
        XCTAssertNotNil(sut.navigationItem.rightBarButtonItem)
    }
    
    func testControllerTitle() {
        XCTAssertEqual(sut.title, "Basket")
    }
    
    func testDidChangeCurrency() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let currencyListSut = storyboard.instantiateViewController(withIdentifier: "SelectCurrencyViewController") as! SBSelectCurrencyViewController
        
        XCTAssertEqual(sut.dataSource.basket.currency, SBCurrency.USD())
        
        currencyListSut.dataSource.setList(list: SBCurrencyList(items: [SBCurrency.PLN(), SBCurrency.USD()]))
        
        currencyListSut.delegate = sut
        currencyListSut.tableView(currencyListSut.currencyView().currencyTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(sut.dataSource.basket.currency, SBCurrency.PLN())
    }
    
    func testButtonShouldBeDisableWhenQuantityZero() {
        sut.viewWillAppear(true)
        XCTAssertFalse(sut.productListView().checkoutButton.isEnabled)
    }
    
    func testButtonShouldBeEnableWhenQuantityMoreThenZero() {
        let productList = sut.dataSource.list as! SBProductList
        let product = productList.item(indexPath: IndexPath(row: 0, section: 0)) as! SBProduct
        product.quantity += 1

        sut.didChangeQuantity(product)
        XCTAssertTrue(sut.productListView().checkoutButton.isEnabled)
    }
}
