import XCTest
@testable import Shopping_Basket

class SBSummaryViewControllerTests: XCTestCase {

    var sut: SBSummaryViewController!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "SummaryViewController") as! SBSummaryViewController

        _ = sut.view
    }
    
    func testControllerComponentsNotNil() {
        XCTAssertNotNil(sut.summaryView().payButton)
        XCTAssertNotNil(sut.summaryView().cancelButton)
    }
    
    func testButtonTitleShouldHasTotalPrice() {
        sut.totalPrice = 15.15
        let basket = SBBasket()
        basket.currency = SBCurrency.PLN()
        sut.setupButton()
        XCTAssertEqual(sut.summaryView().payButton.titleLabel?.text, "Pay \(17.02) \(basket.currency.code)")
    }
    
}
