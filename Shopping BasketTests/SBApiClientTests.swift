import XCTest
@testable import Shopping_Basket

class SBApiClientTests: XCTestCase {
    
    func testParseCurencies() {
        let curreniesJSON = [ "USDPLN" : Float(1.321), "USDUSD" : Float(1.0)]
        let currenies = SBCurrency.parseJSON(json: curreniesJSON)
        
        let firstCurrency = currenies.first!
        let lastCurrency = currenies.last!
        
        XCTAssertEqual(firstCurrency.code, "PLN")
        XCTAssertEqual(firstCurrency.exchangeRate, 1.321)
        
        XCTAssertEqual(lastCurrency.code, "USD")
        XCTAssertEqual(lastCurrency.exchangeRate, 1)
    }
    
}
