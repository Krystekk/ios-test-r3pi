import XCTest
@testable import Shopping_Basket

class SBBasketTests: XCTestCase {
    
    var basket: SBBasket!
    
    override func setUp() {
        basket = SBBasket()
    }
    
    func testBasketShouldHasDefaultUSD() {
        XCTAssertTrue(basket.currency == SBCurrency.USD())
        XCTAssertFalse(basket.currency == SBCurrency.PLN())
    }
    
    func testBasketCanChangeDefaultCurrency() {
        basket.currency = SBCurrency.PLN()
        XCTAssertTrue(basket.currency == SBCurrency.PLN())
    }
    
    func testBasketConvertCurrency() {
        basket.currency = SBCurrency.PLN()
        XCTAssertEqual(basket.convertUSDToCurrentCurrency(price: 1), 1.12)
        
        basket.currency = SBCurrency(code: "CHF", exchangeRate: 0.9)
        XCTAssertEqual(basket.convertUSDToCurrentCurrency(price: 3.25), 2.93)
        
        basket.currency = SBCurrency(code: "CRC", exchangeRate: 564.340027)
        XCTAssertEqual(basket.convertUSDToCurrentCurrency(price: 2.99), 1687.38)
    }
    
    func testPriceShouldBeConveted() {
        let product = SBProduct(name: "test", unit: "abc", price: 1.2)
        XCTAssertEqual(basket.productPrice(product: product), "1.2 USD per abc")
        basket.currency = SBCurrency.PLN()
        XCTAssertEqual(basket.productPrice(product: product), "1.35 PLN per abc")
    }

}

extension SBCurrency {
    static func PLN() -> SBCurrency {
        return SBCurrency(code: "PLN", exchangeRate: 1.123213)
    }
}
