import XCTest
@testable import Shopping_Basket

class SBProductListTests: XCTestCase {
    
    func testProductListNotEmpty() {
        XCTAssertEqual(SBProductList.foodList().count(), 4)
    }
    
    func testProductListEmpty() {
        XCTAssertEqual(SBProductList.emptyList().count(), 0)
    }
    
    func testListAttributesNotNil() {
        let productList = SBProductList.emptyList()
        productList.attributes = SBListAttributes(columns: 4, margin: 8.0)

        XCTAssertEqual(productList.columns(), 4)
        XCTAssertEqual(productList.margin(), 8)
    }
    
    func testListAttributesEmpty() {
        let productList = SBProductList.emptyList()
        
        XCTAssertEqual(productList.columns(), 0)
        XCTAssertEqual(productList.margin(), 0)
    }
    
    func testTotalPrice() {
        let productOne = SBProduct(name: "First", unit: "ABC", price: 1.0)
        productOne.quantity = 2
        
        let productTwo = SBProduct(name: "Second", unit: "CBA", price: 2.0)
        productTwo.quantity = 2
        
        let productList = SBProductList(items: [productOne, productTwo])
        XCTAssertEqual(productList.totalPrice(), 6.0)
    }
    
}
