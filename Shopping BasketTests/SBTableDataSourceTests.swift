import XCTest
@testable import Shopping_Basket

class SBTableDataSourceTests: XCTestCase {
    
    var dataSource: SBTableDataSource!
    var tableView: UITableView!
    
    override func setUp() {
        dataSource = SBTableDataSource()
        tableView = UITableView()
    }
    
    func testReturnFourRowsForFourCurrency() {
        let currencyList = SBCurrencyList(items: [SBCurrency(code: "AED", exchangeRate: 3.6727), SBCurrency(code: "PLN", exchangeRate: 1.2727)])
        dataSource.setList(list: currencyList)
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 2)
    }
    
    func testReturnZeroRowForEmptyCurrency() {
        dataSource.setList(list: SBCurrencyList.emptyList())
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 0)
    }
    
}
